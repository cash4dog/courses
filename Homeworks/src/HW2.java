import java.util.Random;
import java.util.Scanner;

public class HW2 {
    public static void main(String[] args) {
        int rows = 5;
        int columns = 5;

        char[][] targetBoard = new char[rows][columns];
        for (int i = 0; i < targetBoard.length; i++) {
            for (int j = 0; j < targetBoard[i].length; j++) {
                targetBoard[i][j] = '-';
            }
        }

        Random random = new Random();
        int targetRow = random.nextInt(rows);
        int targetColumn = random.nextInt(columns);
        //  targetBoard[targetRow][targetColumn] = 'x';  //test
        //  printBoard(targetBoard);  //test

        System.out.println("All Set. Get ready to rumble!");

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print("Enter row for shooting (1-5): ");
            int shotRow = getUserInput(scanner, rows);

            System.out.print("Enter column for shooting (1-5): ");
            int shotColumn = getUserInput(scanner, columns);

            if (shotRow - 1 == targetRow && shotColumn - 1 == targetColumn) {
                System.out.println("You have won!");
                targetBoard[shotRow - 1][shotColumn - 1] = 'x';
                printBoard(targetBoard);
                break;
            } else {
                System.out.println("Missed! Try again.");
                targetBoard[shotRow - 1][shotColumn - 1] = '*';
                printBoard(targetBoard);
            }
        }

        scanner.close();
    }

    private static int getUserInput(Scanner scanner, int max) {
        int userInput;
        while (true) {
            try {
                userInput = Integer.parseInt(scanner.nextLine());
                if (userInput >= 1 && userInput <= max) {
                    break;
                } else {
                    System.out.println("Please enter a number within the valid range.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter a number.");
            }
        }
        return userInput;
    }

    private static void printBoard(char[][] board) {
        System.out.println("0 | 1 | 2 | 3 | 4 | 5 |");
        for (int i = 1; i <= board.length; i++) {
            System.out.print(i + " | ");
            for (int j = 1; j <= board[i - 1].length; j++) {
                System.out.print(board[i - 1][j - 1] + " | ");
            }
            System.out.println();
        }
    }
}