package HW4;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Pet dog = new Pet("Dog", "Lucky", 5, 75, new String[]{"eat", "drink", "sleep"});
        System.out.println(dog.toString());
        String[][] schedule = {{"Day1","Wake up"},{"Day2","Go to work"}};
        Human olena = new Human("Olena", "Zelenska",1980,80,dog,schedule);
        System.out.println(olena.toString());
        Human oleh = new Human("Oleh","Zelensky",1978,85,dog,schedule);
        System.out.println(oleh.toString());
        Family original = new Family(olena,oleh,dog);
        System.out.println(original.toString());
        Human max = new Human("Max","Zelenskiy",2023);
        original.addChild(max);
        System.out.println(original.toString());
        original.deleteChild(0);
        System.out.println(original.toString());
        Family futureFamily = new Family();
        Pet cat = new Pet("Cat","Sandra",12,45,new String[]{"eat","meow","sleep"});
        Human zhenya = new Human("Zhenya","Velikiy",1988,90,cat,schedule,futureFamily);
        Human natasha = new Human("Natasha","Velikaya",1990,88,cat,schedule,futureFamily);
        futureFamily.setFather(zhenya);
        futureFamily.setMother(natasha);
        Human zlata = new Human("Zlata","Velikaya",2022);
        Human zlataBig = new Human("Zlata","Velikaya",2022);
        System.out.println(zlataBig.equals(zlata));
        System.out.println(futureFamily.toString());
        futureFamily.setPet(cat);
        futureFamily.addChild(zlata);
        zlata.setFamily(futureFamily);
        zlata.setPet(cat);
        zlata.setIq(50);
        zlata.setSchedule(schedule);
        zlata.setYear(2020);
        zlata.setName("Zlatusya");
        zlata.setSurname("Velik");
        System.out.println(zlata.getFamily());
        System.out.println(zlata.getName());
        System.out.println(zlata.getSurname());
        System.out.println(zlata.getYear());
        System.out.println(zlata.getIq());
        zlata.printSchedule();
        System.out.println(zlata.getPet());
        System.out.println(zlata.getClass());
        zlata.toString();
        zlata.greetPet();
        zlata.describePet();
        zlata.feedPet();
        cat.toString();
        cat.setSpecies("Tiger");
        cat.setNickname("Murka");
        cat.setAge(10);
        cat.setTrickLevel(99);
        System.out.println(cat.getSpecies());
        System.out.println(cat.getNickname());
        System.out.println(cat.getAge());
        System.out.println(cat.getTrickLevel());
        System.out.println(Arrays.toString(cat.getHabits()));
        System.out.println(cat.getClass());
        cat.respond();
        cat.eat();
        cat.foul();
        System.out.println(cat.toString());

    }
}
