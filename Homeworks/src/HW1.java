import java.util.Random;
import java.util.Scanner;

public class HW1 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Please, enter your name: ");
        String name = input.nextLine();

        System.out.println("Let the game begin!");

        Random rn = new Random();
        int randomNum = rn.nextInt(101);

        while (true) {
            System.out.print("Enter your number from 0 to 100: ");
            int userNum = input.nextInt();

            if (userNum == randomNum) {
                System.out.println("Congratulations, " + name + "!");
                break; }
            else if (userNum > randomNum)
                System.out.println("Your number is too big. Please, try again.");
            else if (userNum < randomNum)
                System.out.println("Your number is too small. Please, try again.");
            }


        }

        }
