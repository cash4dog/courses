import java.util.Scanner;

public class HW3 {

            public static void main(String[] args) {
            String[][] schedule = new String[7][2];

            schedule[0][0] = "Sunday";
            schedule[0][1] = "do home work";
            schedule[1][0] = "Monday";
            schedule[1][1] = "go to courses; watch a film";
            schedule[2][0] = "Tuesday";
            schedule[2][1] = "go to the hospital; buy some medicine";
            schedule[3][0] = "Wednesday";
            schedule[3][1] = "go to courses; see classmate Jack";
            schedule[4][0] = "Thursday";
            schedule[4][1] = "go to supermarket; do home work";
            schedule[5][0] = "Friday";
            schedule[5][1] = "go to courses; have a supper in the restaurant";
            schedule[6][0] = "Saturday";
            schedule[6][1] = "see my family";

            Scanner scanner = new Scanner(System.in);

            while (true) {
                System.out.print("Please, input the day of the week: ");
                String userInput = scanner.nextLine().trim().toLowerCase();

                switch (userInput) {
                    case "exit":
                        System.out.println("Exiting the program. Goodbye!");
                        return;
                    case "monday","tuesday","wednesday","thursday","friday","saturday","sunday":
                        int dayIndex = getDayIndex(userInput, schedule);
                        System.out.println("Your tasks for " + schedule[dayIndex][0] + ": " + schedule[dayIndex][1]);
                        continue;
                    default:
                        System.out.println("Sorry, I don't understand you, please try again.");
                }
                }
            }

            public static int getDayIndex (String userInput, String[][] schedule) {
                int dayIndex = 0;
                for (int i = 0; i < schedule.length; i++) {
                    if (userInput.equalsIgnoreCase(schedule[i][0])) {
                        dayIndex = i;
                        break;
                    }
                }
                return dayIndex;
            }
        }
