package Homeworks.src.HW6;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
class FamilyTest {

        @Test
        void testToString() {
            Human mother = new Human("Anna", "Johnson", 30);
            Human father = new Human("John", "Johnson", 35);
            Family family = new Family(mother, father);

            assertEquals("Family{mother=Human{name='Anna', surname='Johnson', year=30, iq=0, schedule=null}, father=Human{name='John', surname='Johnson', year=35, iq=0, schedule=null}, children=[], pet=null}", family.toString());
        }

        @Test
        void testDeleteChildByObject() {
            Human mother = new Human("Anna", "Johnson", 30);
            Human father = new Human("John", "Johnson", 35);
            Family family = new Family(mother, father);

            Human child1 = new Human("Alice", "Johnson", 10);
            Human child2 = new Human("Bob", "Johnson", 8);

            family.addChild(child1);
            family.addChild(child2);
            System.out.println(family.toString());

            assertTrue(family.deleteChild(child1));
            assertFalse(Arrays.asList(family.getChildren()).contains(child1));

            assertFalse(family.deleteChild(new Human("Charlie", "Johnson", 5))); // Non-existent child
            assertEquals(1, family.getChildren().length); // Array remains unchanged
        }

        @Test
        void testDeleteChildByIndex() {
            Human mother = new Human("Anna", "Johnson", 30);
            Human father = new Human("John", "Johnson", 35);
            Family family = new Family(mother, father);

            Human child1 = new Human("Alice", "Johnson", 10);
            Human child2 = new Human("Bob", "Johnson", 8);

            family.addChild(child1);
            family.addChild(child2);

            assertTrue(family.deleteChild(1));
            assertEquals(1, family.getChildren().length);
            assertFalse(Arrays.asList(family.getChildren()).contains(child2));

            assertFalse(family.deleteChild(2)); // Invalid index
            assertEquals(1, family.getChildren().length); // Array remains unchanged
        }

        @Test
        void testAddChild() {
            Human mother = new Human("Anna", "Johnson", 30);
            Human father = new Human("John", "Johnson", 35);
            Family family = new Family(mother, father);

            Human child = new Human("Alice", "Johnson", 10);
            family.addChild(child);

            assertEquals(1, family.getChildren().length);
            assertEquals(child, family.getChildren()[0]);
            assertEquals(family, child.getFamily());
        }

        @Test
        void testCountFamily() {
            Human mother = new Human("Anna", "Johnson", 30);
            Human father = new Human("John", "Johnson", 35);
            Family family = new Family(mother, father);

            Human child1 = new Human("Alice", "Johnson", 10);
            Human child2 = new Human("Bob", "Johnson", 8);

            family.addChild(child1);
            family.addChild(child2);

            assertEquals(4, family.countFamily()); // 2 parents + 2 children
        }
    }
class PetTest {

    @Test
    void testFish() {
        Fish fish = new Fish("Nemo");
        assertEquals(Species.FISH, fish.getSpecies());
        assertEquals("Nemo", fish.getNickname());

        fish.respond(); // This should print "..."
    }

    @Test
    void testDomesticCat() {
        DomesticCat cat = new DomesticCat("Whiskers");
        assertEquals(Species.CAT, cat.getSpecies());
        assertEquals("Whiskers", cat.getNickname());

        cat.respond(); // This should print "Мур-мяу! Я - Whiskers."
        cat.foul();    // This should print "Кішка сходила в лоток."
    }

    @Test
    void testDog() {
        Dog dog = new Dog("Buddy");
        assertEquals(Species.DOG, dog.getSpecies());
        assertEquals("Buddy", dog.getNickname());

        dog.respond(); // This should print "Гав-гав! Я Buddy."
        dog.foul();    // This should print "Пес наклав на газон."
    }

    @Test
    void testRoboCat() {
        RoboCat roboCat = new RoboCat("R2D2");
        assertEquals(Species.ROBOCAT, roboCat.getSpecies());
        assertEquals("R2D2", roboCat.getNickname());

        roboCat.respond(); // This should print "Я робо-кіт R2D2!"
        roboCat.foul();    // This should print "Робо-кіт не справляє природні потреби."
    }
}
class HumanTest {

    @Test
    void testGreetPetMan() {
        Man man = new Man();
        man.setFamily(new Family());
        man.getFamily().setPet(new Dog("Buddy", 3, 80, new String[]{"Fetch"}));

        man.greetPet(); // This should print "Овва, привіт, Buddy!"

    }

    @Test
    void testGreetPetWoman() {
        Woman woman = new Woman();
        woman.setFamily(new Family());
        woman.getFamily().setPet(new DomesticCat("Whiskers", 2, 60, new String[]{"Nap"}));

        woman.greetPet(); // This should print "Привітик, Whiskers!"
    }

    @Test
    void testRepairCar() {
        Man man = new Man();
        man.repairCar(); // This should print "Знову ця бляшанка зламалась, зараз я їй переберу гайки..."
    }

    @Test
    void testMakeUp() {
        Woman woman = new Woman();
        woman.makeUp(); // This should print "Потрібно навести марафет!"
    }
}
