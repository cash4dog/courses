package Homeworks.src.HW6;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children = new Human[0];
    private Pet pet;

    Family () {}

    Family(Human mother, Human father) {
        setMother(mother);
        mother.setFamily(this);
        setFather(father);
        father.setFamily(this);
        setChildren(new Human[0]);
        }

    Family (Human mother, Human father, Pet pet) {
        this (mother, father);
        setPet(pet);
        }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void addChild(Human child) {
        Human[] newChildren = Arrays.copyOf(children, children.length + 1);
            newChildren[children.length] = child;
            setChildren(newChildren);
            child.setFamily(this);
        }

    public boolean deleteChild(int index) {
        if (index >= 0 && index < children.length) {
            Human[] newChildren = new Human[children.length - 1];
            System.arraycopy(children, 0, newChildren, 0, index);
            System.arraycopy(children, index + 1, newChildren, index, children.length - index - 1);
            setChildren(newChildren);
            return true;
        } else {
            return false;
        }
    }

    public boolean deleteChild(Human child) {
        for (int i = 0; i < children.length; i++) {
            if (children[i].equals(child)) {
                Human[] newChildren = new Human[children.length - 1];
                System.arraycopy(children, 0, newChildren, 0, i);
                System.arraycopy(children, i + 1, newChildren, i, children.length - i - 1);
                setChildren(newChildren);
                return true;
            }
        }
        return false;
    }
    public int countFamily() {
        return 2 + children.length;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father) && Arrays.equals(children, family.children) && Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Об'єкт Family з матір'ю " + getMother() + " та батьком " + getFather() + " видаляється.");
        super.finalize();
    }
}

