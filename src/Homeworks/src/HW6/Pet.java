package Homeworks.src.HW6;

import java.util.Arrays;
import java.util.Objects;

enum Species {
    FISH,
    CAT,
    DOG,
    ROBOCAT,
    UNKNOWN
}

interface Foul {
    public void foul();
//    {
//        System.out.println("Потрібно добре замести сліди...");
//    }
}
public abstract class Pet {

    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    Pet () {}

    Pet (String nickname) {
        setNickname(nickname);
    }

    Pet (String nickname, int age, int trickLevel, String[] habits) {
        this(nickname);
        setAge(age);
        setTrickLevel(trickLevel);
        setHabits(habits);
    }


    public void setSpecies (Species species) {
        this.species = species;
    }
    public Species getSpecies () {
        return species;
    }

    public void setNickname (String nickname) {
        this.nickname = nickname;
    }

    public String getNickname () {
        return nickname;
    }

    public void setAge (int age) {
        this.age = age;
    }

    public int getAge () {
        return age;
    }

    public void setTrickLevel(int trickLevel) {
        if (trickLevel >= 0 && trickLevel <= 100) {
            this.trickLevel = trickLevel;
        } else {
            System.out.println("Помилка: рівень хитрості має бути в межах від 0 до 100.");
        }
    }

    public int getTrickLevel () {
        return trickLevel;
    }

    public void setHabits (String[] habits) {
        this.habits = habits;
    }

    public String[] getHabits () {
        return habits;
    }

    public void eat() {
        System.out.println("Я їм!");
    }

    public abstract void respond();
//    {
//        System.out.println("Привіт, хазяїн. Я - " + getNickname() + ". Я скучив!");
//    }

    @Override
    public String toString() {
        return getSpecies() + "{nickname='" + getNickname() + "', age=" + getAge() + ", trickLevel=" + getTrickLevel() + ", habits=" + Arrays.toString(getHabits()) + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age && Objects.equals(species, pet.species) && Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Об'єкт Pet з нікнеймом '" + getNickname() + "' видаляється.");
        super.finalize();
    }
}

class Fish extends Pet {

    Fish() {
        super();
        setSpecies(Species.UNKNOWN);
    }
    Fish(String nickname) {
        super(nickname);
        setSpecies(Species.FISH);
    }

    Fish(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.FISH);
    }
    @Override
    public void respond() {
        System.out.println("...");
    }

}

class DomesticCat extends Pet implements Foul {
    DomesticCat() {
        super();
        setSpecies(Species.UNKNOWN);
    }
    DomesticCat(String nickname) {
        super(nickname);
        setSpecies(Species.CAT);
    }

    DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.CAT);
    }
    @Override
    public void respond() {
        System.out.println("Мур-мяу! Я - " + getNickname() + ".");
    }

    @Override
    public void foul() {
        System.out.println("Кішка сходила в лоток.");
    }
}

class Dog extends Pet implements Foul {
    Dog() {
        super();
        setSpecies(Species.UNKNOWN);
    }
    Dog(String nickname) {
        super(nickname);
        setSpecies(Species.DOG);
    }

    Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.DOG);
    }

    @Override
    public void respond() {
        System.out.println("Гав-гав! Я " + getNickname() + ".");
    }

    @Override
    public void foul() {
        System.out.println("Пес наклав на газон.");
    }
}

class RoboCat extends Pet implements Foul {
    RoboCat() {
        super();
        setSpecies(Species.UNKNOWN);
    }
    RoboCat(String nickname) {
        super(nickname);
        setSpecies(Species.ROBOCAT);
    }

    RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.ROBOCAT);
    }
    @Override
    public void respond() {
        System.out.println("Я робо-кіт " + getNickname() + "!");
    }

    @Override
    public void foul() {
        System.out.println("Робо-кіт не справляє природні потреби.");
    }
}