package Homeworks.src.HW6;

public class Main {
    public static void main(String[] args) {
//        for (int i = 0; i < 100_000; i++) {
//            Human human = new Human();
//        }
        String[][] schedule1 = {{DayOfWeek.MONDAY.name(),"Wake up"},{DayOfWeek.TUESDAY.name(),"Go to work"}};
        Human person = new Human("John", "Week", 25,75,schedule1);
        person.printSchedule();

        Pet cat1 = new Fish();
        Pet dog1 = new Dog("Bob");
        System.out.println(cat1.getSpecies());
        System.out.println(dog1.getSpecies());
    }
}
