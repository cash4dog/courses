package Homeworks.src.HW5;

import java.util.Arrays;
import java.util.Objects;

enum Species {
    CAT,
    DOG,
    FISH,
    RABBIT
}

public class Pet {

    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    Pet () {};

    Pet (Species species, String nickname) {
        this.species = species;
        setNickname(nickname);
    }

    Pet (Species species, String nickname, int age, int trickLevel, String[] habits) {
        this(species,nickname);
        setAge(age);
        setTrickLevel(trickLevel);
        setHabits(habits);
    }



    public Species getSpecies () {
        return species;
    }

    public void setNickname (String nickname) {
        this.nickname = nickname;
    }

    public String getNickname () {
        return nickname;
    }

    public void setAge (int age) {
        this.age = age;
    }

    public int getAge () {
        return age;
    }

    public void setTrickLevel(int trickLevel) {
        if (trickLevel >= 0 && trickLevel <= 100) {
            this.trickLevel = trickLevel;
        } else {
            System.out.println("Помилка: рівень хитрості має бути в межах від 0 до 100.");
        }
    }

    public int getTrickLevel () {
        return trickLevel;
    }

    public void setHabits (String[] habits) {
        this.habits = habits;
    }

    public String[] getHabits () {
        return habits;
    }

    public void eat() {
        System.out.println("Я їм!");
    }

    public void respond() {
        System.out.println("Привіт, хазяїн. Я - " + getNickname() + ". Я скучив!");
    }

    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }

    @Override
    public String toString() {
        return getSpecies() + "{nickname='" + getNickname() + "', age=" + getAge() + ", trickLevel=" + getTrickLevel() + ", habits=" + Arrays.toString(getHabits()) + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age && Objects.equals(species, pet.species) && Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Об'єкт Pet з нікнеймом '" + getNickname() + "' видаляється.");
        super.finalize();
    }
}
