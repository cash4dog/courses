package Homeworks.src.HW5;

import java.util.Arrays;
import java.util.Objects;

enum DayOfWeek {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY
}

public class Human {
        private String name;
        private String surname;
        private int year;
        private int iq;
        private String[][] schedule;
        private Family family;

    Human () {};

    Human (String name, String surname, int year) {
        setName(name);
        setSurname(surname);
        setYear(year);
    }

    Human (String name, String surname, int year, int iq, String[][] schedule) {
        this(name, surname, year);
        setIq(iq);
        setSchedule(schedule);
    }

    Human (String name, String surname, int year, int iq, String[][] schedule, Family family, Pet pet) {
        this(name, surname, year, iq, schedule);
        setFamily(family);
        pet = this.getFamily().getPet();

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        if (iq >= 0 && iq <= 100) {
            this.iq = iq;
        } else {
            System.out.println("Помилка: рівень IQ має бути в межах від 0 до 100.");
        }
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void printSchedule () {
            System.out.println(Arrays.deepToString(getSchedule()));
    }


    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        // Перевіряємо, чи батько вже належить до іншої сім'ї
        if (this.family == null) {
            this.family = family;
            //family.addChild(this); // додаємо поточну людину до сім'ї
        } else {
            System.out.println("Помилка: Ця людина вже належить до іншої сім'ї.");
        }
    }

    public void greetPet() {
            System.out.println("Привіт, " + this.getFamily().getPet().getNickname());
        }

    public void describePet() {
            System.out.println("У мене є " + this.getFamily().getPet().getSpecies() + ", їй " + this.getFamily().getPet().getAge() + " років, він " + (this.getFamily().getPet().getTrickLevel() > 50 ? "дуже хитрий" : "майже не хитрий"));
        }

    public void feedPet() {
        System.out.println("Зараз я тебе нагодую, " + this.getFamily().getPet().getNickname());
    }

        @Override
        public String toString() {
            return "Human{name='" + getName() + "', surname='" + getSurname() + "', year=" + getYear() + ", iq=" + getIq() + ", schedule=" + Arrays.deepToString(getSchedule()) + "}";
        }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year && iq == human.iq && Objects.equals(name, human.name) && Objects.equals(surname, human.surname) && Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq, family);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Об'єкт Human з нікнеймом '" + getName() + "' видаляється.");
        super.finalize();
    }
}
