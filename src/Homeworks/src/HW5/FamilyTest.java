package Homeworks.src.HW5;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
public class FamilyTest {

        @Test
        void testToString() {
            Human mother = new Human("Anna", "Johnson", 30);
            Human father = new Human("John", "Johnson", 35);
            Family family = new Family(mother, father);

            assertEquals("Family{mother=Human{name='Anna', surname='Johnson', year=30, iq=0, schedule=null}, father=Human{name='John', surname='Johnson', year=35, iq=0, schedule=null}, children=[], pet=null}", family.toString());
        }

        @Test
        void testDeleteChildByObject() {
            Human mother = new Human("Anna", "Johnson", 30);
            Human father = new Human("John", "Johnson", 35);
            Family family = new Family(mother, father);

            Human child1 = new Human("Alice", "Johnson", 10);
            Human child2 = new Human("Bob", "Johnson", 8);

            family.addChild(child1);
            family.addChild(child2);
            System.out.println(family.toString());

            assertTrue(family.deleteChild(child1));
            assertFalse(Arrays.asList(family.getChildren()).contains(child1));

            assertFalse(family.deleteChild(new Human("Charlie", "Johnson", 5))); // Non-existent child
            assertEquals(1, family.getChildren().length); // Array remains unchanged
        }

        @Test
        void testDeleteChildByIndex() {
            Human mother = new Human("Anna", "Johnson", 30);
            Human father = new Human("John", "Johnson", 35);
            Family family = new Family(mother, father);

            Human child1 = new Human("Alice", "Johnson", 10);
            Human child2 = new Human("Bob", "Johnson", 8);

            family.addChild(child1);
            family.addChild(child2);

            assertTrue(family.deleteChild(1));
            assertEquals(1, family.getChildren().length);
            assertFalse(Arrays.asList(family.getChildren()).contains(child2));

            assertFalse(family.deleteChild(2)); // Invalid index
            assertEquals(1, family.getChildren().length); // Array remains unchanged
        }

        @Test
        void testAddChild() {
            Human mother = new Human("Anna", "Johnson", 30);
            Human father = new Human("John", "Johnson", 35);
            Family family = new Family(mother, father);

            Human child = new Human("Alice", "Johnson", 10);
            family.addChild(child);

            assertEquals(1, family.getChildren().length);
            assertEquals(child, family.getChildren()[0]);
            assertEquals(family, child.getFamily());
        }

        @Test
        void testCountFamily() {
            Human mother = new Human("Anna", "Johnson", 30);
            Human father = new Human("John", "Johnson", 35);
            Family family = new Family(mother, father);

            Human child1 = new Human("Alice", "Johnson", 10);
            Human child2 = new Human("Bob", "Johnson", 8);

            family.addChild(child1);
            family.addChild(child2);

            assertEquals(4, family.countFamily()); // 2 parents + 2 children
        }
    }


